
import static org.junit.Assert.*;
import org.junit.Test;

import java.nio.charset.Charset;
import java.util.Random;

public class HairdresserTest {

    @Test
    public void longerThanIntMaxValue(){
        String s = "2147483647";
        assertEquals(false, Hairdresser.validateInput(s));
    }

    @Test (timeout=1000)
    public void testNegativeInput() {
        boolean b = Hairdresser.validateInput("-10000");
        assertEquals(false, b);
    }
    @Test
    public void testRandomInput() {
        byte[] array = new byte[7]; // length is bounded by 7
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));
        boolean b = Hairdresser.validateInput(generatedString);
        assertEquals(false, b);
    }
    @Test
    public void testSmallAmountOfInhabitats() {
        int i = 500;
        assertEquals(2, Hairdresser.calculateHairdressers(500));
    }
}
