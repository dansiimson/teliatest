public class Hairdresser {
    public static boolean validateInput(String s) {
        String pattern = "([0-9])\\w+";
        if (!s.matches(pattern) || s.length() > 9) {
            return false;
        }
        return true;
    }
    public static int calculateHairdressers(int i) {
        //lets say 50% of inhabitants are men
        int amountOfMen = i/2;
        //if theres less than 350 men
        if (amountOfMen < 350) {
            return 2;
        }
        //lets say 2% of inhabitats visit hairdresser on one day
        double dailyVisitors = amountOfMen * 0.02;
        //one hairdresser serves average 7 customers per day
        double hairdressersNeeded = dailyVisitors / 7;
        //might get sick, add one for safety
        hairdressersNeeded += 1;
        //appointment times might overlap, add 15% more hairdressers
        hairdressersNeeded = hairdressersNeeded + hairdressersNeeded * 0.15;
        return (int) hairdressersNeeded;

    }

    public static void main(String[] args) {
        System.out.println("Enter number of inhabitants in city");
        String s = System.console().readLine();
        if (validateInput(s)) {
            System.out.println("The optimal number of hairdressers needed in city is ");
            System.out.println(calculateHairdressers(Integer.parseInt(s)));
        }
        else {
            System.out.println("Please check input");
        }
    }
}
